
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author DigitallSchool
 */
public class CustomerService {

    private List<Customer> customers;

    {
        try {
            List<String> inputValues
                    = Files.readAllLines(Paths.get("", "SampleData.csv"));

            customers = inputValues.subList(1, inputValues.size())
                    .stream().map(Customer::new).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Customer> getCustomers() {
        return customers;
    }
}
